package landcover

import scala.io.Source
import java.io.File

object LandcoverData {
  private[landcover] val fileLocations = loadLocationsFile("/landcover/files.txt")

  private[landcover] def loadLocationsFile(data: String) = {
    
    val stream = this.getClass.getResourceAsStream(data)
    try {
      Source.fromInputStream(stream).getLines().toList
    } finally {
      stream.close()
    }
  }

  private[landcover] def readTiles(files: List[String]) = files.map { file =>
      TileFolder(file)
  }

  val tiles = readTiles(fileLocations)

}
