package landcover

import java.io.File
import java.io.FileInputStream

import javax.imageio.ImageIO

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD

import ij.io.Opener

case class TileFolder(location: String) {
    val name = location.split("/").reverse.head
}

case class Tile(name: String, bytes: Array[Array[Array[Int]]])

object Landcover {
    
    val conf: SparkConf = new SparkConf()
        .setMaster("local")
        .setAppName("LandCover")
        .set("spark.driver.maxResultSize", "10g")
        .set("spark.storage.memoryFraction", "0")
        
        /*
        .set("spark.driver.memory", "10g")
        .set("spark.driver.extraJavaOptions", "8g")
        .set("spark.driver.maxResultSize", "8g")
        .set("spark.worker.memory", "8g")
        .set("spark.executor.memory", "8g")
        .set("spark.executor.extraJavaOptions", "8g")
		*/
    println(conf.toDebugString)
    val sc: SparkContext = new SparkContext(conf)
    val files: List[String] = List("/Users/alex/Desktop/data/LC80240292015244LGN00")/*,
            "/Users/alex/Desktop/data/LC80240292015324LGN00")*/
    val folders: RDD[TileFolder] = sc.parallelize(files.map(file => TileFolder(file)))
    val bands: List[String] = List("1", "2", "3", "4", "5", "6", "7", "9")
    //, "QA"
    
    def readBand(location: String): Array[Array[Int]] = {
        println("\tBand: " + location)
        val opener = new Opener()
        val lala = opener.openImage(location)
        val imageProcessor = lala.getProcessor
        val array = imageProcessor.getIntArray
        println("\tBand " + location.substring(location.length() - 6, location.length() - 4) + " ok")
        array
    }
    
    def readTile(tileFolder: TileFolder): Tile = {
        println("Tile: " + tileFolder.name)
        var bytes: Array[Array[Array[Int]]] = new Array[Array[Array[Int]]](bands.length)
        for(band_index <- 0 until bands.length) {
            bytes(band_index) = readBand(tileFolder.location + "/" + tileFolder.name + "_B" + bands(band_index) + ".TIF")
        }
        Tile(tileFolder.name, bytes)
    }
    
    def readTilesBands(folders: RDD[TileFolder]): RDD[Tile] = {
        println("Reading bands")
        folders.map(folder => readTile(folder))
    }
  
    def main(args: Array[String]): Unit = {
        val lala = readTilesBands(folders).take(1)(0)
        println(lala.bytes(0)(0)(0))
        println(lala.bytes(0)(4000)(4000))
    }
}