name := "landcover"

version := "1.0"

scalaVersion := "2.10.5"

libraryDependencies ++= Seq(
    "org.apache.spark" %% "spark-core" % "1.5.1" % "provided",
    "org.apache.spark" % "spark-mllib_2.10" % "1.5.1" % "provided",
    "org.scalatest" % "scalatest_2.10" % "2.0" % "test",
    "junit" % "junit" % "4.8.1" % "test"
)

libraryDependencies += "gov.nih.imagej" % "imagej" % "1.46"
