Clustering project for land cover types.

The idea is to work with data from [Landsat 8](http://landsat.usgs.gov/landsat8.php), and use it for clustering.

We can then use data from the [National Land Cover Database 2011](http://www.mrlc.gov/nlcd2011.php) to have a ground truth to compare to.

For tests, we are going to use : path 24 row 29 (shawano lake)